package com.proyectosimio.tacitascounter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * 
 * @author Igor �vila Exp�sito
 *
 */
public class TacitasMain extends Activity {

	// Variables que almacenar�n el tiempo inicial y el n�mero de tazas
	// acumuladas.
	private int brewTime, numTazas;

	// TextViews que mostrar�n el tiempo restante y el n�mero de tazas
	// acumuladas.
	private TextView tvTime, tvNumTazas;

	// Bot�n para iniciar y parar el contador.
	private Button btStart;

	// Variable que indicar� si la aplicaci�n est� o no en marcha.
	private boolean working;

	// Cron�metro de la aplicaci�n.
	private CountDownTimer timer;

	// Objeto que reproducir� nuestra alarma
	private MediaPlayer mp;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tacitas_main);

		// Iniciamos e instanciamos todas las variables.
		numTazas = 0;
		brewTime = 3;
		working = false;

		// Inicializamos el reproductor.
		mp = MediaPlayer.create(this, R.raw.starwars);

		// Establecemos las referencias a los elementos del layout que vamos a
		// utilizar en la aplicaci�n.
		btStart = (Button) findViewById(R.id.btStart);
		tvNumTazas = (TextView) findViewById(R.id.numTazas);
		tvTime = (TextView) findViewById(R.id.tvTime);
	}

	/**
	 * M�todo que recibe el evento click de todos los botones
	 * 
	 * @param v
	 */
	public void onClick(View v) {

		// Selector que actuar� en funci�n al bot�n que llame al evento.
		switch (v.getId()) {
		// En el caso de que lo llame el bot�n m�s.
		case R.id.btMas:
			if (brewTime < 10 && !working) {
				brewTime++;
				tvTime.setText(String.valueOf(brewTime));
			}
			break;
		// En el caso de que lo llame el bot�n menos.
		case R.id.btMenos:
			if (brewTime > 1 && !working) {
				brewTime--;
				tvTime.setText(String.valueOf(brewTime));
			}
			break;
		// En el caso de que lo llame el bot�n start.
		case R.id.btStart:
			// Comprobamos si la aplicaci�n est� o no en marcha.
			if (!working) {
				aTrabajar();

			} else {

				// En el caso de que la aplicaci�n s� que est� funcionando, la
				// pausamos, cancelamos el timer y actualizamos el texto del
				// bot�n start.
				working = false;
				timer.cancel();
				btStart.setText("Start");

			}
			break;
		}
	}

	/**
	 * M�todo que se lanzar� para iniciar nuestro descanso.
	 */
	private void aTrabajar() {
		// Si no esta funcionando, la iniciamos.
		working = true;

		// Cambiamos el texto del bot�n start.
		btStart.setText("Stop");

		// Iniciamos el timer, como par�metros pasaremos el n�mero de
		// minutos que hemos establecido en la aplicaci�n, multiplicado
		// por 60 y por 1000 para obtener el valor en milisegundos, el
		// seg�ndo par�metro es el que nos dir� cu�ndo se produce el
		// "tick", tambi�n en milisegundos.
		timer = new CountDownTimer(brewTime * 60 * 1000, 1000) {
			// Al declarar un nuevo CountDownTimer nos obliga a
			// sobreescribir algunos de sus eventos.
			@Override
			public void onTick(long millisUntilFinished) {
				// Este m�todo se lanza por cada lapso de tiempo
				// transcurrido,
				tvTime.setText(String.valueOf(millisUntilFinished / 1000) + "s");
			}

			@Override
			public void onFinish() {
				// Este m�todo se lanza cuando finaliza el contador.

				// Indicamos que la aplicaci�n ya no est� funcionando.
				working = false;

				// Aumentamos el n�mero de "tacitas" que nos hemos
				// tomado, esto es opcional, si uno se quiere tomar un
				// litro de caf� o ToroRojo es libre.
				numTazas++;

				// Actualizamos los textos de las etiquetas y del bot�n,
				// como el par�metro que recibe el m�todo .setText es
				// del tipo String, y el n�mero de tazas lo tenemos
				// almacenado en int, utilizamos la clase String para
				// extraer el valor del int y convertirlo en el tipo
				// String.
				tvNumTazas.setText(String.valueOf(numTazas));
				tvTime.setText("Brew Up!");
				btStart.setText("Start");

				// Comenzamos la reproducci�n de nuestro archivo de sonido.
				mp.start();

				// Creamos el Dialog que se mostrar� para poder pausar la
				// reproducci�n del sonido.
				AlertDialog.Builder builder = new AlertDialog.Builder(
						TacitasMain.this);
				// Le damos un texto al t�tulo del di�logo.
				builder.setTitle("Alarma");
				// Establecemos un mensaje a mostrar al usuario.
				builder.setMessage("�Desea parar la alarma?");
				// Creamos un bot�n y escribimos su evento "onClick".
				builder.setPositiveButton("Aceptar", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Cuando se pulse el bot�n, se parar� la reproducci�n
						// del archivo.
						mp.stop();

					}
				});
				// Una vez creado el Dialog, lo mostramos.
				builder.show();
			}
		};

		// Una vez configurado el timer, lo iniciamos.
		timer.start();

	}

}
