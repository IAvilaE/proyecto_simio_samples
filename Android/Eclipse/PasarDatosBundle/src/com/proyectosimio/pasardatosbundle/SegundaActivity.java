package com.proyectosimio.pasardatosbundle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SegundaActivity extends Activity {

	// TextView que hemos establecido en el Layout.
	private TextView tvMostrarDatos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.segunda_activity);
		// Establecemos la referencia al TextView para poder utilizarlo en el
		// c�digo.
		tvMostrarDatos = (TextView) findViewById(R.id.tvMostrarDatos);

		// Sacamos el intent con el que se ha iniciado la Activity.
		Intent i = getIntent();

		// Del intent sacamos el Bundle en el que hemos introducido los datos en
		// la primera Activity.
		Bundle b = i.getExtras();

		// Comprobamos que el Bundle contenga datos, para evitar posibles
		// errores. Si no lo comprobamos y el Intent no tiene incorporado un
		// bundle, al intentar utilizar el bundle despu�s nos saltar� una
		// excepci�n por intentar un objeto que no existe
		// (NullPointerException).
		if (b != null) {
			String datos = b.getString("datos");

			// Establecemos el texto del TextView a partir de la cadena de texto
			// que hemos sacado del Bundle.
			tvMostrarDatos.setText(datos);

			// Se puede hacer la asignaci�n directamente:
			tvMostrarDatos.setText(getIntent().getExtras().getString("datos"));
		}
	}
}
