package com.proyectosimio.pasardatosbundle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PasarDatosBundleMain extends Activity {

	// Necesitamos declarar la variable para poder utilizar el EditText que
	// hemos inclu�do en el Layout, lo instanciamos aqu� para que el objeto sea
	// utilizable en toda la clase. Es recomendable instanciar en esta zona
	// todas las variables que queramos utilizar a lo largo de nuestro c�digo,
	// de manera que quede ordenado y sea f�cilmente localizable nuestra
	// variable.
	EditText etDatos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pasar_datos_bundle_main);

		// Establecemos la referencia al EditText del Layout, esto lo hacemos
		// buscando en R.java el ID del elemento. Lo hacemos aqu� porque este
		// m�todo se va a lanzar con la ejecuci�n de nuestra Activity, as�
		// siempre estar� inicializado antes de que vayamos a hacer algo en la
		// aplicaci�n.
		etDatos = (EditText) findViewById(R.id.etDatos);
	}

	/**
	 * M�todo al que el bot�n hace referencia en su evento "onClick". En el
	 * Layout hemos indicado que apunte a este m�todo.
	 * 
	 * @param v
	 */
	public void onClick(View v) {

		// Comprobamos que el EditText contenga algo escrito, de no tener nada
		// escrito no har� nada cuando se pulse el bot�n.
		if (etDatos.getText().length() != 0) {

			// Utilizamos un objeto de la clase Bundle para incluir un par
			// "Clave/Valor", este objeto tendr� como clave "datos", y su valor
			// ser� el texto que se introduzca en el EditText.
			Bundle b = new Bundle();
			b.putString("datos", etDatos.getText().toString());

			// La clase Intent establece un link entre esta Activity y la nueva
			// que queremos lanzar, para ello al instanciar el Intent
			// introducimos como par�metros esta propia Activity, y la clase que
			// representa a la nueva Activity.
			Intent i = new Intent(PasarDatosBundleMain.this,
					SegundaActivity.class);

			// En el Intent a�adimos el Bundle, para que lleve la informaci�n a
			// la siguiente Activity.
			i.putExtras(b);

			// Lanzamos la siguiente Activity.
			startActivity(i);
		}
	}
}
