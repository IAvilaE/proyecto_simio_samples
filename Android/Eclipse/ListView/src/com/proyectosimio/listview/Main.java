package com.proyectosimio.listview;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Main extends Activity implements OnItemClickListener {
	private ArrayList<Animal> animales;
	private ListView lvAnimales;
	private AnimalesAdapter adapter;
	private TextView tvNombre, tvNumCelda;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// Inicializamos las variables.
		animales = new ArrayList<Animal>();

		rellenarArrayList();

		tvNombre = (TextView) findViewById(R.id.tvItemContent);
		tvNumCelda = (TextView) findViewById(R.id.tvItemField);

		adapter = new AnimalesAdapter(this, animales);

		lvAnimales = (ListView) findViewById(R.id.lvItems);
		// Asignamos el Adapter al ListView, en este punto hacemos que el
		// ListView muestre los datos que queremos.
		lvAnimales.setAdapter(adapter);
		// Asignamos el Listener al ListView para cuando pulsamos sobre uno de
		// sus items.
		lvAnimales.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * M�todo que rellena el ArrayList con los animales que queremos mostrar en
	 * el ListView.
	 */
	private void rellenarArrayList() {
		animales.add(new Animal("aguila", R.drawable.aguila));
		animales.add(new Animal("ballena", R.drawable.ballena));
		animales.add(new Animal("caballo", R.drawable.caballo));
		animales.add(new Animal("camaleon", R.drawable.camaleon));
		animales.add(new Animal("canario", R.drawable.canario));
		animales.add(new Animal("cerdo", R.drawable.cerdo));
		animales.add(new Animal("delfin", R.drawable.delfin));
		animales.add(new Animal("gato", R.drawable.gato));
		animales.add(new Animal("iguana", R.drawable.iguana));
		animales.add(new Animal("lince", R.drawable.lince));
		animales.add(new Animal("lobo", R.drawable.lobo_9));
		animales.add(new Animal("morena", R.drawable.morena));
		animales.add(new Animal("orca", R.drawable.orca));
		animales.add(new Animal("perro", R.drawable.perro));
		animales.add(new Animal("vaca", R.drawable.vaca));
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long ID) {
		// Al hacer click sobre uno de los items del ListView mostramos los
		// datos en los TextView.
		tvNombre.setText(animales.get(position).getNombre());
		tvNumCelda.setText(String.valueOf(position));

	}

}
