package com.proyectosimio.listview;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Clase que almacena las referencias a los controles de la vista personalizada
 * del ListView.
 * 
 * @author ProyectoSimio
 * 
 */
public class AnimalesHolder {
	public ImageView imgAnimal;
	public TextView tvField, tvContent;
}