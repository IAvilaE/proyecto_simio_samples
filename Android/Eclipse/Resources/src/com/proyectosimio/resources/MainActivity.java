package com.proyectosimio.resources;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Obtenemos todos los recursos de nuestra aplicación.
		Resources resources = getResources();
		
		// Sacamos el string que da nombre a nuestra aplicación.
		String texto = resources.getString(R.string.app_name);
		
		// Obtenemos el shape definido en nuestra aplicación.
		Drawable drawable = resources.getDrawable(R.drawable.buttonshape);
		
		// Obtenemos la referencia al color definido en nuestra aplicación.
		int color = resources.getColor(R.color.btnStyleColor);
		
		// Obtenemos el margen que definimos en nuestra aplicación.
		float dimension = resources.getDimension(R.dimen.btn_margin_top);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
