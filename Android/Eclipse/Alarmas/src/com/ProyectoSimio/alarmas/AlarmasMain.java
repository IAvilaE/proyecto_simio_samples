package com.ProyectoSimio.alarmas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Actividad que lee y escribe ficheros de texto de la memoria interna del
 * dispositivo y utiliza un temporizador para lanzar una alarma visual.
 * 
 * @author ProyectoSimio
 * 
 */
public class AlarmasMain extends Activity {

	private final static String TAG = "Alarmas";

	private final static String FILE = "alarmas.txt";

	private EditText et1Alarma, et2Alarma, et3Alarma, et4Alarma, et5Alarma;
	private TextView tvTiempoRestante, tvAlarmRestantes;

	private ArrayList<Integer> tiempos;
	private int numAlarmas, numAlarmaActual;

	boolean alarmaFinalizada; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		instanciarComponentes();
		
		instanciarVariables();

		establecerTiempoInicial();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v) {

		if (et1Alarma.getText().length() != 0
				&& et2Alarma.getText().length() != 0
				&& et3Alarma.getText().length() != 0
				&& et4Alarma.getText().length() != 0
				&& et5Alarma.getText().length() != 0) {

			tiempos.add(Integer.parseInt(et1Alarma.getText().toString()));
			tiempos.add(Integer.parseInt(et2Alarma.getText().toString()));
			tiempos.add(Integer.parseInt(et3Alarma.getText().toString()));
			tiempos.add(Integer.parseInt(et4Alarma.getText().toString()));
			tiempos.add(Integer.parseInt(et5Alarma.getText().toString()));

			actualizarArchivoAlarmas();

			lanzarAlarma();
		}
	}

	private void instanciarComponentes() {
		et1Alarma = (EditText) findViewById(R.id.et1Alarma);
		et2Alarma = (EditText) findViewById(R.id.et2Alarma);
		et3Alarma = (EditText) findViewById(R.id.et3Alarma);
		et4Alarma = (EditText) findViewById(R.id.et4Alarma);
		et5Alarma = (EditText) findViewById(R.id.et5Alarma);
		tvAlarmRestantes = (TextView) findViewById(R.id.tvAlarmRestantes);
		tvTiempoRestante = (TextView) findViewById(R.id.tvTiempoRestante);
	}

	private void instanciarVariables() {
		tiempos = new ArrayList<Integer>();
		numAlarmas = 0;
		numAlarmaActual = 0;

		alarmaFinalizada = true;
	}

	private void establecerTiempoInicial() {
        // Comprobamos si existe el archivo
        if (existsFile(FILE)) {
            // En el caso de que exista, intentamos rellenar los EditText, si no
            // se rellenan de forma correcta, el archivo.txt estaba corrupto.
            if (!rellenarEditTexts()) {
                // Avisamos al usuario de que el archivo era corrupto.
                Toast.makeText(AlarmasMain.this,
                        "Archivo corrupto, reiniciando par�metros...",
                        Toast.LENGTH_LONG).show();
                // Creamos de nuevo el archivo.
                crearArchivoAlarmas();
                // Rellenamos los EditText con los valores asignados por defecto
                // en el archvio txt.
                rellenarEditTexts();
            }
 
        } else {
            // En el caso de que no existiera el archivo txt lo creamos y rellenamos
            // los EditText.
            crearArchivoAlarmas();
            rellenarEditTexts();
        }
    }
 
    private void crearArchivoAlarmas() {
        try {
            // Creamos un objeto OutputStreamWriter, que ser� el que nos permita
            // escribir en el archivo de texto. Si el archivo no exist�a se crear�
            // autom�ticamente.
            OutputStreamWriter outSWMensaje = new OutputStreamWriter(
                    openFileOutput(FILE, Context.MODE_PRIVATE));
            // Escribimos los 5 tiempos iniciales en el archivo.
            outSWMensaje.write("5\n5\n5\n5\n5\n");
            // Cerramos el flujo de escritura del archivo, este paso es obligatorio,
            // de no hacerlo no se podr� acceder posteriormente al archivo.
            outSWMensaje.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
 
    private boolean rellenarEditTexts() {
        try {
            // Creamos un objeto InputStreamReader, que ser� el que nos permita
            // leer el contenido del archivo de texto.
            InputStreamReader archivo = new InputStreamReader(
                    openFileInput(FILE));
            // Creamos un objeto buffer, en el que iremos almacenando el contenido
            // del archivo.
            BufferedReader br = new BufferedReader(archivo);
            // Por cada EditText leemos una l�nea y escribimos el contenido en el
            // EditText.
            String texto = br.readLine();
            et1Alarma.setText(texto);
 
            texto = br.readLine();
            et2Alarma.setText(texto);
 
            texto = br.readLine();
            et3Alarma.setText(texto);
 
            texto = br.readLine();
            et4Alarma.setText(texto);
 
            texto = br.readLine();
            et5Alarma.setText(texto);
            // Cerramos el flujo de lectura del archivo.
            br.close();
            return true;
 
        } catch (Exception e) {
            return false;
        }
    }

    private void actualizarArchivoAlarmas() {
        try {
            OutputStreamWriter outSWMensaje = new OutputStreamWriter(
                    openFileOutput(FILE, Context.MODE_PRIVATE));
            // Por cada tiempo escrito en los EditText escribimos una l�nea
            // en el archivo de alarmas.
            for (int i : tiempos) {
                outSWMensaje.write(i + "\n");
            }
     
            outSWMensaje.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            Toast.makeText(this, "No se pudo crear el archivo de alarmas",
                    Toast.LENGTH_LONG).show();
        }
    }
     
    private void lanzarAlarma() {
        // Sacamos el tiempo restante para lanzar la alarma actual.
        int i = tiempos.get(numAlarmaActual);
        // Controlamos la alarma que tiene que sonar
        numAlarmaActual++;
        // Variable para controlar cu�ndo ha finalizado una alarma, para poder
        // lanzar la siguiente.
        alarmaFinalizada = false;
        // Actualizamos el texto de las alarmas restantes.
        tvAlarmRestantes.setText("Alarmas restantes: " + numAlarmas);
        // Instanciamos un nuevo contador, que se lanzar� con el tiempo debido.
        CountDownTimer timer = new CountDownTimer(i * 60 * 1000, 100) {
     
            @Override
            public void onTick(long millisUntilFinished) {
                // Actualizamos el tiempo restante para la nueva alarma.
                tvTiempoRestante.setText(String
                        .valueOf(millisUntilFinished / 1000) + "s");
            }
     
            @Override
            public void onFinish() {
                // Al finalizar reproducimos un archivo de audio guardado en la
                // carpeta raw.
                MediaPlayer mp = MediaPlayer.create(AlarmasMain.this, R.raw.r2d2);
                mp.start();
                // Creamos un AlertDialog que avise de la llegada de la alarma.
                AlertDialog.Builder alertDialog;
                alertDialog = new AlertDialog.Builder(AlarmasMain.this);
                alertDialog.setTitle("Alarma");
                alertDialog.setMessage("Es la hora!!");
                // Creamos un bot�n en el AlertDialog, que ser� el que nos permita
                // reanudar el tiempo de la siguiente alarma.
                alertDialog.setPositiveButton("Ok", new OnClickListener() {
     
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cuando se pulse el bot�n se marca la alarma como
                        // finalizada, se actualizan las alarmas restantes y
                        // en el caso de no ser la �ltima alarma se vuelve a
                        // ejecutar el m�todo lanzarAlarma.
                        alarmaFinalizada = true;
                        numAlarmas--;
                        tvAlarmRestantes.setText("Alarmas restantes: "
                                + numAlarmas);
                        if (numAlarmaActual < tiempos.size())
                            lanzarAlarma();
                    }
                });
                // Mostramos el AlertDialog al usuario.
                alertDialog.show();
            }
        };
        // Una vez creado el temporizador lo iniciamos.
        timer.start();
    }
    
    public boolean existsFile(String fileName) {
        for (String tmp : fileList()) {
            if (tmp.equals(fileName))
                return true;
        }
        return false;
    }
}
