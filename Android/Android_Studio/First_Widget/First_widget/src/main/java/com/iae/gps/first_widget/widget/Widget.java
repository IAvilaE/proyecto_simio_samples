package com.iae.gps.first_widget.widget;

import android.app.Activity;
import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.iae.gps.first_widget.R;

/**
 * Created by Igor Ávila Expósito on 17/05/2014.
 */
public class Widget extends AppWidgetProvider {
    private final static String TAG = "Widget";

    /**
     * Este método se ejecuta automáticamente cuando el usuario lo selecciona del listado de widgets
     * para posicionarlo en pantalla, en él vamos a recoger el estado actual del dispositivo, para
     * mostrar el debido icono en el widget.
     *
     * @param cntx
     * @param intent
     */
    @Override
    public void onReceive(Context cntx, Intent intent) {
        RemoteViews updateViews = new RemoteViews(cntx.getPackageName(), R.layout.widget);
        // Crear un manager para poder comprobar el estado del sonido.
        AudioManager audioManager =
                (AudioManager) cntx.getSystemService(Activity.AUDIO_SERVICE);

        // Comprobamos el estado actual y establecemos el icono correcto.
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
            updateViews.setImageViewResource(R.id.ivVolumeState,
                    android.R.drawable.ic_lock_silent_mode);
        } else {
            updateViews.setImageViewResource(R.id.ivVolumeState,
                    android.R.drawable.ic_lock_silent_mode_off);
        }
    }

    @Override
    public void onUpdate(Context cntx, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        cntx.startService(new Intent(cntx, ToggleVolumeService.class));
    }

    /**
     * Clase interna encargada de modificar y actualizar el estado del dispositivo y del widget en
     * pantalla.
     */
    public static class ToggleVolumeService extends IntentService {

        /**
         * Necesario el constructor por motivos de depuración, si no se añadiera el sistema daría
         * una excepción. El String que se le pasa al super es el nombre de nuestra clase
         * AppWidgetProvider, seguido del símbolo dólar y por último el nombre de esta clase
         * interna.
         */
        public ToggleVolumeService() {
            super("Widget$ToggleVolumeService");
        }

        /**
         * Encargado de manejar el intent que se le pase al servicio. En este caso recibirá el
         * intent que se ha lanzado desde los métodos del widget. Como el Intent lo hemos lanzado
         * directamente para ejecutar esta tarea, no nos es necesario analizar ningún dato para
         * seleccionar una tarea u otra.
         *
         * @param intent
         */
        @Override
        public void onHandleIntent(Intent intent) {
            // El objeto ComponentName será el que nos permita comunicarnos con el propio widget
            // desde el servicio.
            ComponentName cn = new ComponentName(this, Widget.class);
            // Creamos un manager para abrir la conexión con la vista remota (el propio widget en
            // pantalla.
            AppWidgetManager mngr = AppWidgetManager.getInstance(this);

            // Actualizamos el estado del widget.
            mngr.updateAppWidget(cn, buildUpdate(this));
        }

        /**
         * Este método crea una instancia del widget (RemoteViews) y actualiza el estado tanto del
         * sonido como del propio widget.
         *
         * @param cntx
         * @return
         */
        private RemoteViews buildUpdate(Context cntx) {
            // Crear una instancia del widget.
            RemoteViews updateViews = new RemoteViews(cntx.getPackageName(), R.layout.widget);
            // Crear un manager para poder modificar el estado del sonido.
            AudioManager audioManager =
                    (AudioManager) cntx.getSystemService(Activity.AUDIO_SERVICE);

            // Comprobamos si el estado actual es el modo vibración o no.
            if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
                // Si estaba en vibración establecemos el estado normal.
                updateViews.setImageViewResource(R.id.ivVolumeState,
                        android.R.drawable.ic_lock_silent_mode_off);
                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            } else {
                // Si estaba el estado normal, establecemos el modo vibración.
                updateViews.setImageViewResource(R.id.ivVolumeState,
                        android.R.drawable.ic_lock_silent_mode);
                audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
            }

            // Creamos un intent que se comunique con el widget.
            Intent intent = new Intent(this, Widget.class);
            // Creamos un PendingIntent que se lanzará cuandos se pulse sobre el widget.
            PendingIntent pi = PendingIntent.getBroadcast(cntx, 0, intent, 0);
            // Establecemos el PendingIntent que se debe lanzar al pulsar sobre el widget.
            updateViews.setOnClickPendingIntent(R.id.ivVolumeState, pi);

            return updateViews;
        }
    }
}
