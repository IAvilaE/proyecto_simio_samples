package com.proyectosimio.gson;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proyectosimio.gson.models.Flickr;

import java.text.SimpleDateFormat;


public class MainActivity extends ActionBarActivity {
    private final static String TAG = "MainActivity";
    private final static String jsonDocument = "{" +
            "        \"title\": \"Uploads from everyone\"," +
            "        \"link\": \"http://www.flickr.com/photos/\"," +
            "        \"description\": \"\"," +
            "        \"modified\": \"2015-02-09T19:15:27Z\"," +
            "        \"generator\": \"http://www.flickr.com/\"," +
            "        \"items\": [" +
            "       {" +
            "            \"title\": \"Wasserschloss Hamburg\"," +
            "            \"link\": \"http://www.flickr.com/photos/130271032@N08/15862510884/\"," +
            "            \"media\": {\"m\":\"http://farm8.staticflickr.com/7379/15862510884_4ffb5c39c1_m.jpg\"}," +
            "            \"date_taken\": \"2015-02-09T11:15:27-08:00\"," +
            "            \"description\": \" <p><a href=\\\"http://www.flickr.com/people/130271032@N08/\\\">Alex _Lex</a> posted a photo:</p> <p><a href=\\\"http://www.flickr.com/photos/130271032@N08/15862510884/\\\" title=\\\"Wasserschloss Hamburg\\\"><img src=\\\"http://farm8.staticflickr.com/7379/15862510884_4ffb5c39c1_m.jpg\\\" width=\\\"240\\\" height=\\\"160\\\" alt=\\\"Wasserschloss Hamburg\\\" /></a></p> \"," +
            "            \"published\": \"2015-02-09T19:15:27Z\"," +
            "            \"author\": \"nobody@flickr.com (Alex _Lex)\"," +
            "            \"author_id\": \"130271032@N08\"," +
            "            \"tags\": \"hamburg\"" +
            "       }," +
            "       {" +
            "            \"title\": \"P2089751-2.jpg\"," +
            "            \"link\": \"http://www.flickr.com/photos/punimoe/15862511764/\"," +
            "            \"media\": {\"m\":\"http://farm8.staticflickr.com/7450/15862511764_2955c62652_m.jpg\"}," +
            "            \"date_taken\": \"2015-02-08T15:36:31-08:00\"," +
            "            \"description\": \" <p><a href=\\\"http://www.flickr.com/people/punimoe/\\\">punimoe<\\/a> posted a photo:<\\/p> <p><a href=\\\"http://www.flickr.com/photos/punimoe/15862511764/\\\" title=\\\"P2089751-2.jpg\\\"><img src=\\\"http://farm8.staticflickr.com/7450/15862511764_2955c62652_m.jpg\\\" width=\\\"180\\\" height=\\\"240\\\" alt=\\\"P2089751-2.jpg\\\" /></a></p> <p>Olympus digital camera</p>\"," +
            "            \"published\": \"2015-02-09T19:15:33Z\"," +
            "            \"author\": \"nobody@flickr.com (punimoe)\"," +
            "            \"author_id\": \"23881764@N02\"," +
            "            \"tags\": \"\"" +
            "       }]" +
            "}";
    private final static String DATE_PATTERN = "yyyy-MM-dd'T'hh:mm:ss";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // En el método setDateFormat pasamos el pattern de nuestras fechas. Se añade
        // disableHtmlEscaping() para evitar parseo de símbolos html.
        Gson gson = new GsonBuilder().setDateFormat(DATE_PATTERN).disableHtmlEscaping().serializeNulls().create();

        Flickr flickr = gson.fromJson(jsonDocument, Flickr.class);
        Log.d(TAG, new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(flickr.getModified()));

        String json = gson.toJson(flickr);

        Log.d(TAG, json);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
