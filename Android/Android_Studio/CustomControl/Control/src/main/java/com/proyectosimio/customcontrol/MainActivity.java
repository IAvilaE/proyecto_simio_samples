package com.proyectosimio.customcontrol;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Proyecto Simio on 03/10/2014.
 */
public class MainActivity extends Activity {
    private Slider slider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        slider = (Slider) findViewById(R.id.slider);
    }

    public void onClickPrevButton(View v) {
        slider.showPrevText();
    }

    public void onClickNextButton(View v) {
        slider.showNextText();
    }
}
