//
//  ViewController2.swift
//  Navegacion
//
//  Created by Iván Díaz Molina on 7/2/15.
//  Copyright (c) 2015 Iván Díaz Molina. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet var lblNombre: UILabel!
    var nombre:String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.lblNombre.text = "Hola " + self.nombre;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
