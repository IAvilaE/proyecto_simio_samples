//
//  ViewController.swift
//  Navegacion
//
//  Created by Iván Díaz Molina on 7/2/15.
//  Copyright (c) 2015 Iván Díaz Molina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var tfNombre: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var vista:ViewController2 = segue.destinationViewController as ViewController2
        vista.nombre = self.tfNombre.text;
    }


}

