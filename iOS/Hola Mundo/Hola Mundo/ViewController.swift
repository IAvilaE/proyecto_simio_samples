//
//  ViewController.swift
//  Hola Mundo
//
//  Created by Iván Díaz Molina on 7/2/15.
//  Copyright (c) 2015 Iván Díaz Molina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var lblNombre: UILabel!
    @IBOutlet var tfNombre: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onClickSaludar(sender: UIButton) {
        if(!self.tfNombre.text.isEmpty) {
            self.lblNombre.text = "Hola " + self.tfNombre.text;
        }
    }

}

