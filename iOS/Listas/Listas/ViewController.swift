//
//  ViewController.swift
//  Listas
//
//  Created by Iván Díaz Molina on 15/2/15.
//  Copyright (c) 2015 Iván Díaz Molina. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var lvContactos: UITableView!
    var contactos = Array<Contacto>();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initContactos();
        self.lvContactos.delegate = self;
        self.lvContactos.dataSource = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactos.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var celda:ListItemContacto = self.lvContactos.dequeueReusableCellWithIdentifier("celda") as ListItemContacto;
        celda.lblNombre.text = self.contactos[indexPath.row].nombre;
        celda.lblTelefono.text = String(self.contactos[indexPath.row].telefono);
        return celda;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        NSLog("Has seleccionado la celda %d", indexPath.row);
    }
    
    func initContactos () {
        //Contacto 1
        var tmp:Contacto = Contacto();
        tmp.nombre = "Iván Díaz";
        tmp.telefono = 123456789;
        self.contactos.append(tmp);
        
        //Contacto 2
        tmp = Contacto();
        tmp.nombre = "Igor Ávila";
        tmp.telefono = 987654321;
        self.contactos.append(tmp);
        
    }
}

