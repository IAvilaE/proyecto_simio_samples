//
//  ListItemContacto.swift
//  Listas
//
//  Created by Iván Díaz Molina on 15/2/15.
//  Copyright (c) 2015 Iván Díaz Molina. All rights reserved.
//

import UIKit

class ListItemContacto: UITableViewCell {

    @IBOutlet var lblNombre: UILabel!
    @IBOutlet var lblTelefono: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
